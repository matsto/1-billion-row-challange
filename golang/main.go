package main

import (
	"os"
)


func main() {
	file, err := os.Open("temperatures.csv")
	if err != nil {
		panic(err)

	}
	defer file.Close()

	app := NewApp()
	app.Run(file)

}
