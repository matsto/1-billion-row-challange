package main

import (
	"fmt"
	"io"
	"strconv"
	"sync"
)

const (
	dateLenWithEnvelope = 16
	extractorCount      = 64
	queueLen            = 64
	chunkSize           = 1024 * 5
)

type Controller struct {
	chunkChan chan []byte
	errCh     chan error
	channels  map[byte]chan reading
	out       chan map[string]*temperature
}

func NewController() *Controller {
	channels := make(map[byte]chan reading, uint8('Z'-'A')+1)
	// prepare channels
	var letter byte
	for letter = 'A'; letter <= 'Z'; letter++ {
		channels[letter] = make(chan reading, queueLen)
	}

	return &Controller{
		chunkChan: make(chan []byte, extractorCount*queueLen),
		errCh:     make(chan error),
		channels:  channels,
		out:       make(chan map[string]*temperature, len(channels)),
	}
}

func (ctrl *Controller) Read(file io.ReadSeekCloser) {
	buf := make([]byte, chunkSize)
	chunk := make([]byte, 0, chunkSize*2) // Create a buffer with enough space
	reminder := make([]byte, 0, chunkSize)

	file.Seek(38, 0) //skip header
	var lastNewline int

	for {
		// Read the next chunk into the buffer
		n, err := file.Read(buf)
		if err == io.EOF {
			break
		}
		if err != nil {
			ctrl.errCh <- err
			return
		}

		// Reset chunk and append reminder and new data
		chunk = chunk[:0] // Reset chunk without reallocating
		chunk = append(chunk, reminder...)
		chunk = append(chunk, buf[:n]...)

		// Find the last newline character
		lastNewline = -1
		for i := len(chunk) - 1; i >= 0; i-- {
			if chunk[i] == '\n' {
				lastNewline = i
				break
			}
		}

		if lastNewline != -1 {
			chunkCopy := make([]byte, len(chunk[:lastNewline]))
			copy(chunkCopy, chunk[:lastNewline])

			ctrl.chunkChan <- chunkCopy                               // Send a copy to avoid race conditions
			reminder = append(reminder[:0], chunk[lastNewline+1:]...) // Copy the remainder
		} else {
			// No newline found, keep the entire chunk as a reminder
			reminder = append(reminder[:0], chunk...)
		}

	}

	// Send any remaining reminder data
	if len(reminder) > 0 {
		ctrl.chunkChan <- reminder
	}
	close(ctrl.chunkChan)

}

func (ctrl *Controller) ExtractRowData() {
	var wgW sync.WaitGroup

	wgW.Add(extractorCount)
	for i := 0; i < extractorCount; i++ {
		go ctrl.extractRowData(&wgW)
	}
	go func() {
		wgW.Wait()
		for _, c := range ctrl.channels {
			close(c) // Close channels after sending all data
		}
	}()
}

func (ctrl *Controller) extractRowData(wgW *sync.WaitGroup) {
	defer wgW.Done()
	commas := make([]int, 2)

	var (
		ok           bool
		c            chan reading
		commaCounter int
		val          []byte // temporary value used to read from row
		temp         float64
		err          error
		tmp          float32
		r            reading
		chunk        []byte
		newLineIndex int
		i            int
		l            byte
	)
	for chunk = range ctrl.chunkChan {
		newLineIndex = 0
		for newLineIndex < len(chunk) {
			commaCounter = 0
			for i, l = range chunk[newLineIndex:] {
				if l != ',' {
					continue
				}
				commas[commaCounter] = i + newLineIndex
				commaCounter++

				if commaCounter == 2 {
					break
				}
			}
			if commaCounter < 2 {
				ctrl.errCh <- fmt.Errorf("invalid row: %s", chunk)
				return
			}
			for i, l = range chunk[commas[1]+dateLenWithEnvelope:] {
				if l == '\n' {
					newLineIndex = i + commas[1] + dateLenWithEnvelope
					break
				}
				if i == len(chunk[commas[1]+dateLenWithEnvelope:])-1 {
					newLineIndex = len(chunk)
					break
				}
			}

			val = chunk[commas[1]+dateLenWithEnvelope-1 : newLineIndex-2]
			temp, err = strconv.ParseFloat(string(val), 32)
			if err != nil {
				ctrl.errCh <- err
				return
			}
			tmp = float32(temp)

			val = chunk[commas[0]+2 : commas[1]-1]

			r = reading{string(val), tmp}

			c, ok = ctrl.channels[chunk[commas[0]+2]]
			if !ok {
				ctrl.channels['X'] <- r
				continue
			}
			c <- r
		}
	}
}

func (ctrl *Controller) ProduceAggregates() {
	var wg sync.WaitGroup

	wg.Add(len(ctrl.channels))
	for _, ch := range ctrl.channels {
		go ctrl.produceAggregates(ch, &wg)
	}
	go func() {
		wg.Wait()         // Wait for workers to finish
		close(ctrl.out)   // Close out channel after all workers are done
		close(ctrl.errCh) // Close error channel after all workers are done
	}()
}

func (ctrl *Controller) produceAggregates(ch chan reading, wg *sync.WaitGroup) {
	defer wg.Done()
	temp_records := map[string]*temperature{}

	var (
		err     error
		t       *temperature
		ok      bool
		reading reading
	)

	for reading = range ch {
		t, ok = temp_records[reading.location]
		if !ok {
			t, err = newTemperature(reading.temp)
			if err != nil {
				ctrl.errCh <- err
				return
			}
			temp_records[reading.location] = t
			continue
		}
		err = t.fromRow(reading.temp)
		if err != nil {
			ctrl.errCh <- err
			return
		}
	}

	ctrl.out <- temp_records

}

func (ctrl *Controller) GetError() error {
	return <-ctrl.errCh
}

func (ctrl *Controller) PrintOutput() {
	for maps := range ctrl.out {
		for location, temp := range maps {
			fmt.Printf("%s: %.2f/%.2f/%.2f;\n", location, temp.min_temp, temp.max_temp, temp.avg())
		}
	}
}
