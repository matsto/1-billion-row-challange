package main

type temperature struct {
	min_temp float32
	max_temp float32
	sum_temp float32
	count    uint32
}
type record struct {
	location string
	data     *temperature
	avg      float32
}

type reading struct {
	location string
	temp     float32
}

func newTemperature(tmp float32) (*temperature, error) {
	return &temperature{min_temp: tmp, max_temp: tmp, sum_temp: tmp, count: 1}, nil
}

func (t *temperature) fromRow(tmp float32) error {
	if tmp < t.min_temp {
		t.min_temp = tmp
	} else if tmp > t.max_temp {
		t.max_temp = tmp
	}
	t.sum_temp += tmp
	t.count++
	return nil
}

func (t *temperature) avg() float32 {
	return t.sum_temp / float32(t.count)
}
