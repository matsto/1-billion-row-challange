package main

import "io"

type App struct {
	controller *Controller
}

func NewApp() *App {
	return &App{
		controller: NewController(),
	}
}
func (app *App) Run(in io.ReadSeekCloser) {

	// read source file
	go app.controller.Read(in)

	// extract row data
	app.controller.ExtractRowData()

	// workers
	app.controller.ProduceAggregates()

	// errors
	go func() {
		err := app.controller.GetError()
		if err != nil {
			panic(err)
		}
	}()

	app.controller.PrintOutput()
}
