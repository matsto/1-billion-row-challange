from faker import Faker
import csv
from tqdm import tqdm

fake = Faker('en_GB')

records_no = 50_000_000

fields = ('id', 'location', 'date',  'temperature')

with open('temperatures.csv', 'w', newline='') as csvfile:
    writer = csv.DictWriter(csvfile, fieldnames=fields, quoting= csv.QUOTE_ALL)
    writer.writeheader()
    for i in tqdm(range(records_no)):
        writer.writerow(
            {
                "id": i,
                "location": fake.city(),
                "date": fake.date(),
                "temperature": fake.random_int(-2000, 4000)/100,
            }
        )
