import csv
from collections import defaultdict
from dataclasses import dataclass

FIELDS = ("id", "location", "date", "temperature")


@dataclass
class Location:
    min: float = float("inf")
    max: float = float("-inf")
    sum: float = 0.0
    count: int = 0

    def from_row(self, row: dict[str, float]):
        temp = float(row["temperature"])
        self.min = min(self.min, temp)
        self.max = max(self.max, temp)
        self.sum += temp
        self.count += 1

    @property
    def average(self):
        return self.sum / self.count


def read(csvfile) -> dict[str, Location]:
    locations = defaultdict(Location)
    with open(csvfile, "r") as f:
        reader = csv.DictReader(f, fieldnames=FIELDS)
        reader.__next__()
        for row in reader:
            locations[row["location"]].from_row(row)
    return locations


def print_stats(locations: dict[str, Location]):
    print(
        ";\n".join(
            (
                f"{location}: {stats.min:.2f}/{stats.max:.2f}/{stats.average:.2f}"
                for location, stats in locations.items()
            )
        )
    )


if __name__ == "__main__":
    print_stats(read("temperatures.csv"))
