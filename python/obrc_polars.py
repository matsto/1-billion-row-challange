import polars as pl

lf = pl.scan_csv("temperatures.csv")


t = "temperature"


df = (
    lf.select(pl.col("location"), pl.col(t).cast(pl.Float32).alias(t))
    .filter(pl.col(t).is_not_null())
    .group_by("location")
    .agg(
        pl.col(t).min().alias("min"),
        pl.col(t).max().alias("max"),
        pl.col(t).mean().alias("average"),
    )
    .collect()
)


def print_stats(locations: list[dict[str, str | float]]):
    for stats in locations:
        print(
            f"{stats['location']}: {stats['min']:.2f}/"
            f"{stats['max']:.2f}/{stats['average']:.2f}"
        )


print_stats(df.to_dicts())
