# 1 billion row challange

This repo is to compare a few languages to solve simple aggregation task in large size data.

## Implementations

- Python
    - stdlib - implementation using just a stdlibrary of python serves as a base line
    - polars - uses data engineering project named polars (similar to pandas)
- V - uses just stdlib, this language is fearly inmature, this implementation suffers from memory leaks
- GO - the best implementation, laverages simple concurrency model of the language
