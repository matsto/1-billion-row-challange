module main

import encoding.csv
import math
import strconv
import runtime
import sync

const file_path = 'temperatures.csv'

struct TempRecord {
mut:
	min_temp f32 = 100.0
	max_temp f32 = -100.0
	sum_temp f32
	count    u32
}

fn (mut t TempRecord) from_row(row []string) ! {
	temp := f32(strconv.atof_quick(row[3]))
	t.max_temp = math.max(t.max_temp, temp)
	t.min_temp = math.min(t.min_temp, temp)
	t.sum_temp += temp
	t.count += 1
}

fn (t TempRecord) avg() f32 {
	return t.sum_temp / t.count
}

fn main() {
	shared temp_records := map[string]&TempRecord{}
	no_workers := runtime.nr_cpus()
	ch := chan []string{cap: no_workers * 2}
	err_ch := chan int{}
	mut wg := sync.new_waitgroup()
	defer {
		err_ch.close()
	}
	spawn fn (ch chan []string, err_ch chan int) {
		defer {
			ch.close()
		}
		mut csvr := csv.csv_sequential_reader(
			file_path: file_path
			end_line_len: csv.endline_crlf_len
		) or {
			err_ch <- 1
			return
		}

		defer {
			csvr.dispose_csv_reader()
		}

		if csvr.has_data() > 0 {
			// skip header
			csvr.get_next_row() or {
				err_ch <- 1
				return
			}
		}

		for csvr.has_data() > 0 {
			ch <- csvr.get_next_row() or {
				err_ch <- 1
				return
			}
		}
	}(ch, err_ch)

	for _ in 0 .. no_workers {
		wg.add(1)
		spawn fn (ch chan []string, err_ch chan int, shared temp_records map[string]&TempRecord, mut wg sync.WaitGroup) {
			defer {
				wg.done()
			}
			mut t := &TempRecord{}
			mut new := false
			mut row := []string{}
			for {
				row = <-ch or { return }
				lock temp_records {
					t = temp_records[row[1]] or {
						new = true
						&TempRecord{}
					}
					if new {
						temp_records[row[1]] = t
						new = false
					}
					t.from_row(row) or {
						err_ch <- 1
						break
					}
				}
			}
		}(ch, err_ch, shared temp_records, mut wg)
	}

	spawn fn (err_ch chan int) {
		for {
			n := <-err_ch or { return }

			eprintln('Error: ${n}')
			exit(1)
		}
	}(err_ch)
	wg.wait()

	for loc, rec in temp_records {
		println('${loc}: ${rec.min_temp}/${rec.max_temp}/${rec.avg()};')
	}
}
